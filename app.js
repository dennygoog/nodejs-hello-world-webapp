import express from 'express';

const app = express();

app.get('/', (req, res) => {
  res.send('Hello world!');
});

const port = parseInt(process.env.PORT || '8000');

app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
